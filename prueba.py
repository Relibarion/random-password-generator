from tkinter import * 
import random
import string
window = Tk()
text = Text(window)
window.title("Password generator for security: RND")
window.geometry('720x480')
lbl= Label(window, text="Random password generator for security", font=("Courier New", 16))
lbl.pack()
lb2 = Label(window, text="Important: this software does'nt save your data and thats why \n you need save your password with your own risk", font=("Courier New", 10))
lb2.pack()
lb3 = Label(window, text="choose the length for your password", font=("Courier New", 14))
lb3.pack()
var = DoubleVar()
scale = Scale( window, variable = var,orient = HORIZONTAL, from_=8, to=25, width=18, length=250 )
scale.pack(anchor = CENTER)
def randompwd():
    length=int(var.get())
    random1 = ''.join([random.choice(string.ascii_letters + string.digits + string.punctuation ) for n in range(length)])
    label.config(text=random1)
def copiar_al_portapapeles():
    window.clipboard_clear()
    window.clipboard_append(label.cget("text"))
btn = Button(window, text="Generate password", command=randompwd)    
btn.pack()
label = Label(window, font=("Courier New", 19))
label.pack()
btn2 = Button(window, text="copy to clipboard", command=copiar_al_portapapeles)
btn2.pack()
window.mainloop()
#loop para mantener abierto

